module gitee.com/akimimi/getuigo

go 1.13

require (
	github.com/geek-go/getui v0.0.0-20190825093400-5604668721c2
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/gogap/errors v0.0.0-20200228125012-531a6449b28c // indirect
	github.com/gogap/logs v0.0.0-20150329044033-31c6d1e28b2c
	github.com/gogap/stack v0.0.0-20150131034635-fef68dddd4f8 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
)
